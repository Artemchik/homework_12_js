const buttonList =  document.querySelectorAll(".btn");
const sKey = document.querySelector(".key_s");
const eKey = document.querySelector(".key_e");
const oKey = document.querySelector(".key_o");
const nKey = document.querySelector(".key_n");
const lKey = document.querySelector(".key_l");
const zKey = document.querySelector(".key_z");
const enterKey = document.querySelector(".key_enter");
function classChecker(list){
    list.forEach(elem =>{
        if (elem.classList.contains("pushed") === false){
            elem.classList.remove("switched");
            elem.classList.remove("pushed");
        }else{
            elem.classList.remove("pushed");
            elem.classList.add("switched");
        }
    })
}
function classAdder(item){
    item.classList.add("pushed");
}
document.addEventListener("keydown",function keyPress(event){
    switch  (event.code){
    case "KeyS":
        classChecker(buttonList);
        classAdder(sKey);
        break;
    case "KeyE":
        classChecker(buttonList);
        classAdder(eKey);
        break;
    case "KeyO":
        classChecker(buttonList);
        classAdder(oKey);
        break;
    case "KeyN":
        classChecker(buttonList);
        classAdder(nKey);
        break;
    case "KeyL":
        classChecker(buttonList);
        classAdder(lKey);
        break;
    case "KeyZ":
        classChecker(buttonList);
        classAdder(zKey);
        break;
    case "Enter":
        classChecker(buttonList);
        classAdder(enterKey);
        break;
    default:

        break;
}});